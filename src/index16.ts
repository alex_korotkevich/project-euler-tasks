function makeArr(n: number) {
  let newNumber = n ** 1000
  let a = BigInt(newNumber)
  let sum = a
    .toString()
    .split("")
    .map((item) => +item)
    .reduce((sum, cur) => sum + cur)
  return sum
}

console.log(makeArr(2))

export {}
