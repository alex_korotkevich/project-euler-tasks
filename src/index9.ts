function isTriplet(a: number, b: number, c: number) {
  if (a < b && b < c && a ** 2 + b ** 2 === c ** 2) {
    return true
  }
  return false
}

function findNumbers() {
  for (let i = 0; i < 1000; i++) {
    for (let j = 0; j < 1000; j++) {
      for (let k = 0; k < 1000; k++) {
        if (isTriplet(i, j, k) && i + j + k === 1000) {
          console.log(i * j * k)
        }
      }
    }
  }
}

findNumbers()
