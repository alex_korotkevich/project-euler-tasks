// 5
function isDivisible(a: number): boolean {
  for (let i = 2; i < 21; i++) {
    if (a % i !== 0) {
      return false
    }
  }
  return true
}

function divisible() {
  for (let i = 1; ; i++) {
    if (isDivisible(i)) {
      console.log(i)
      return i
    }
  }
}

divisible()
