function getNext(n: number): number {
  if (n % 2 === 0) {
    return n / 2
  } else {
    return n * 3 + 1
  }
}

function countNumbers(n: number) {
  let count = 0
  for (let i = n; i > 1; i = getNext(i)) {
    count++
  }
  return count
}

let maxLength = 0
let res = 0
for (let i = 1; i < 1_000_000; i++) {
  const length = countNumbers(i)
  if (length > maxLength) {
    maxLength = length
    res = i
  }
}
console.log(res)
