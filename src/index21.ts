function sumOfDivisors(n: number) {
  let arr = []
  for (let i = 1; i < n; i++) {
    if (n % i === 0) {
      arr.push(i)
    }
  }
  const sum = arr.reduce((acc, cur) => acc + cur, 0)
  return sum
}

function isAmicable(res: number) {
  let a = sumOfDivisors(res)
  return sumOfDivisors(a) === res && a !== res
}

function amicableCount() {
  let sum = 0
  for (let i = 1; i < 10000; i++) {
    if (isAmicable(i)) {
      sum += i
    }
  }
  return sum
}
console.log(amicableCount())
