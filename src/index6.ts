function sumSqrs() {
  let sum = 0
  for (let i = 1; i < 101; i++) {
    let sqr = i ** 2
    sum += sqr
  }
  return sum
}

function sqrsSum() {
  let sum = 0
  for (let i = 1; i < 101; i++) {
    sum += i
  }
  return sum ** 2
}

console.log(sqrsSum() - sumSqrs())
