function countDivisors(n: number) {
  let cnt = 0
  for (let i = 1; i <= Math.sqrt(n); i++) {
    if (n % i == 0) {
      if (n / i == i) cnt++
      else cnt = cnt + 2
    }
  }
  return cnt
}

function countFactors() {
  let temp = 0
  for (let i = 1; ; i++) {
    temp += i
    if (countDivisors(temp) > 500) {
      return temp
    }
  }
}

console.log(countFactors())
