const maxNum = 4_000_000
const firstNum = 1
let prevNum = 0
let nextNum: number
let sum = 0

for (let i = firstNum; i <= maxNum; i = nextNum) {
  nextNum = i + prevNum
  prevNum = i
  if (i % 2 === 0) {
    sum += i
  }
}
// console.log(sum)

function isPolindrome(a: number): boolean {
  const str = a.toString()
  const b: string = str.split("").reverse().join("")
  return str === b
}

function multiply() {
  let temporal = 0
  for (let i = 999; i > 99; i--) {
    for (let j = 999; j > 99; j--) {
      let res = i * j
      if (isPolindrome(res) && res > temporal) {
        temporal = res
      }
    }
  }
  console.log(temporal)
}

multiply()

function isDivisible(n: number): boolean {
  for (let i = 2; i < 21; i++) {
    if (n % i !== 0) {
      return false
    }
  }
  return true
}

function divisible() {
  for (let i = 1; ; i++) {
    if (isDivisible(i)) {
      console.log(i)
      return
    }
  }
}
// divisible()

function sumSquares() {
  let sum = 0
  for (let i = 1; i < 101; i++) {
    let sqr = i ** 2
    sum += sqr
  }
  return sum
}

function sqrSum() {
  let sum = 0
  for (let i = 1; i < 101; i++) {
    sum += i
  }
  return sum ** 2
}

// console.log(sumSquares() - sqrSum())

function isPrime(a: number) {
  if (a === 1) return true

  for (let i = 2; i <= Math.ceil(a / 2); i++) {
    if (a % i == 0) {
      return false
    }
  }
  return true
}

function primeCount() {
  let count = 0
  for (let i = 2; ; i++) {
    if (isPrime(i)) {
      count++
    }
    if (count === 10001) {
      return i
    }
  }
}
// console.log(primeCount())

export {}
