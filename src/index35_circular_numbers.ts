const isPrime = (n: number): boolean => {
  for (let i = 2; i <= Math.sqrt(n); i++) {
    if (n % i === 0) {
      return false
    }
  }

  return n > 1
}

const isCircularPrime = (n: number): boolean => {
  let temp = n

  do {
    const arr = temp.toString().split("")
    const first = arr.shift()
    temp = +(arr.join("") + first)
    if (!isPrime(temp) || temp.toString().length !== n.toString().length) {
      return false
    }
  } while (temp !== n)

  return true
}

export const task35 = () => {
  let res = 0
  for (let i = 2; i < 1_000_000; i++) {
    if (isCircularPrime(i)) {
      res++
    }
  }
  console.log("task 35:", res)
}

console.log(task35())
export {}
