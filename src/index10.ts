function isPrimes(a: number): boolean {
  if (a === 1) {
    return true
  }
  for (let i = 2; i * i <= a; i++) {
    if (a % i === 0) {
      return false
    }
  }
  return true
}

function sumOfPrimes() {
  let sum = 2
  for (let i = 3; i < 2000000; i++) {
    if (isPrimes(i)) {
      sum += i
      // console.log(i)
    }
  }
  console.log(sum)
}

sumOfPrimes()
