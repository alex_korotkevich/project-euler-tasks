setInterval(logClockTime, 1000)

function logClockTime() {
  let time = getClockTime()
  console.clear()
  console.log(time)
}

function getClockTime() {
  let date = new Date()
  // let time = ""
  let time = {
    hours: date.getHours(),
    minures: date.getMinutes(),
    seconds: date.getSeconds(),
    ampm: "AM",
  }
  if (time.hours === 12) {
    time.ampm = "PM"
  } else if (time.hours > 12) {
    time.ampm = " PM"
    time.hours -= 12
  }
  if (time.hours < 10) {
    time.hours = +("0" + time.hours)
  }
  if (time.minures < 10) {
    time.minures = +("0" + time.hours)
  }
  if (time.seconds < 10) {
    time.seconds = +("0" + time.hours)
  }

  return time.hours + ":" + time.minures + ":" + time.seconds + ":" + time.ampm
}
