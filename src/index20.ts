function countNumbers(n: number): bigint {
  if (n !== 1) {
    return BigInt(BigInt(n) * countNumbers(n - 1))
  }
  return BigInt(1)
}

function findSumOfMembers(n: bigint): number {
  let sum = n
    .toString()
    .split("")
    .map((item) => +item)
    .reduce((sum, cur) => sum + cur)
  return sum
}

console.log(findSumOfMembers(countNumbers(100)))
export {}
