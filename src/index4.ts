function isPolindrome(a: number): boolean {
  const str = a.toString()
  const b: string = str.split("").reverse().join("")
  return str === b
}

function multiply() {
  let temp = 0
  for (let i = 999; i > 99; i--) {
    for (let j = 999; j > 99; j--) {
      let res = i * j
      if (isPolindrome(res) && res > temp) {
        temp = res
      }
    }
  }
  console.log(temp)
}

multiply()
