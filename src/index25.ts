const fibonacci = (n: number) => {
  let a = 0,
    b = 1,
    c = n

  for (let i = 2; i <= n; i++) {
    c = a + b
    a = b
    b = c
  }

  return c
}
// console.log(fibonacci(12))

function findIndex(): number {
  for (let i = 0; ; i++) {
    let num = fibonacci(i)
    if (num.toString().length === 50) {
      return i
    }
  }
}

console.log(findIndex())
