import { getDivisors } from "./utils/getDivisors"

const LIMIT = 28124

const isAbundant = (num: number) => {
  const sum = getDivisors(num).reduce((acc, cur) => acc + cur, 0)
  return sum > num
}

const getAllAbundant = () => {
  const abundants: number[] = []

  for (let i = 12; i < LIMIT; i++) {
    if (isAbundant(i)) {
      abundants.push(i)
    }
  }

  return abundants
}

export const task23 = () => {
  const abundants: number[] = getAllAbundant()

  const arr = []
  for (let i = 0; i < LIMIT; i++) {
    arr[i] = 0
  }

  for (let i = 0; i < abundants.length; i++) {
    for (let j = i; j < abundants.length; j++) {
      const sumOf2AbundantNums = abundants[i] + abundants[j]
      if (sumOf2AbundantNums < LIMIT) {
        if (arr[sumOf2AbundantNums] === 0) {
          arr[sumOf2AbundantNums] = sumOf2AbundantNums
        }
      }
    }
  }

  let sum = 0
  arr.forEach((item, idx) => {
    if (item === 0) {
      sum += idx
    }
  })

  console.log("task 23:", sum)
}

task23()
