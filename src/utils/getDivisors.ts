export const getDivisors = (num: number): number[] => {
  if (num === 1) return [1]

  const arr = [1]
  for (let i = 2; i <= Math.sqrt(num); i++) {
    if (num % i === 0) {
      const div = num / i
      if (div === i) {
        arr.push(i)
      } else {
        arr.push(i, div)
      }
    }
  }

  return arr
}
