function isPrime(a: number): boolean {
  if (a === 1) {
    return true
  }
  for (let i = 2; i < a; i++) {
    if (a % i === 0) {
      return false
    }
  }
  return true
}

function primeCount(): number {
  let count = 0
  for (let i = 2; ; i++) {
    if (isPrime(i)) {
      count++
    }
    if (count === 10001) {
      return i
    }
  }
}

console.log(primeCount())

export {}
