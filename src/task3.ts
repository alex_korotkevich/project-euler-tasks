function isPrime(n: number) {
  if (n === 1) return true

  for (let i = 2; i <= Math.ceil(n / 2); i++) {
    if (n % i == 0) {
      return false
    }
  }

  return true
}

function getDivisor(n: number) {
  let div = []
  for (let i = 1; i <= Math.sqrt(n); i++) {
    if (n % i == 0) {
      if (n / i == i) {
        div.push(i)
      } else {
        div.push(i, n / i)
      }
    }
  }
  return div
    .filter((item) => isPrime(item))
    .sort((a, b) => a - b)
    .pop()
}

console.log(getDivisor(600851475143))

export {}
